FROM d4science/gcore-distribution

RUN wget --no-check-certificate https://nexus.d4science.org/nexus/content/repositories/gcube-releases/org/gcube/resourcemanagement/vremodeler-service/2.0.6-4.3.0-144804/vremodeler-service-2.0.6-4.3.0-144804-full.gar && gcore-deploy-service vremodeler-service-2.0.6-4.3.0-144804-full.gar

RUN mkdir $HOME/.gcore && chown gcube:gcube $HOME/.gcore
RUN mkdir -p /gcube-data/persisted
RUN ln -s /gcube-data/persisted $HOME/.gcore/