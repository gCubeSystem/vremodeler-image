# VRE Modeler Docker image

This repo contains the Dockerfile to build the image of the gCore VRE Modeler service

## Build the image

```shell
$ docker build -t vremodeler-image .
```

## Test the image

```shell
$ docker container run --name d4science/gcore-vremodeler-image vremodeler-image
```
